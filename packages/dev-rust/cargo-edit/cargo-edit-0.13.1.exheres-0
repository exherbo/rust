# Copyright 2017 Marc-Antoine Perennou <keruspe@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require cargo

SUMMARY="This extends Cargo to allow you to add and list dependencies by reading/writing to your `Cargo.toml` file from the command line"

LICENCES="Apache-2.0 MIT"
SLOT="0"
PLATFORMS="~amd64 ~armv8"

MYOPTIONS="providers: ( libressl openssl ) [[ number-selected = exactly-one ]]"

# The supported LibreSSL and OpenSSL versions are taken from the `version_error()`
# function of openssl-sys 0.9.104, which is what is specified by the Cargo.lock of
# cargo-edit-0.13.1
# See: https://github.com/sfackler/rust-openssl/blob/openssl-sys-v0.9.104/openssl-sys/build/main.rs#L422
DEPENDENCIES="
    build+run:
        providers:libressl? ( dev-libs/libressl:=[>=2.5&<=4.0] )
        providers:openssl?  ( dev-libs/openssl:= )
"

RESTRICT="test"

src_configure() {
    export OPENSSL_NO_VENDOR=1
    cargo_src_configure
}
