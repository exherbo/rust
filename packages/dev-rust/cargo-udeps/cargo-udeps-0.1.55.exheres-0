# Copyright 2020 Marc-Antoine Perennou <keruspe@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require cargo [ rust_minimum_version=1.82 ]

SUMMARY="Find unused dependencies in Cargo.toml."

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64 ~armv8"

MYOPTIONS="providers: ( libressl openssl ) [[ number-selected = exactly-one ]]"

# The supported LibreSSL and OpenSSL versions are taken from the `version_error()`
# function of openssl-sys 0.9.106, which is what is specified by the Cargo.lock of
# cargo-udeps 0.1.55
# See: https://github.com/sfackler/rust-openssl/blob/openssl-sys-v0.9.106/openssl-sys/build/main.rs#L422
DEPENDENCIES="
    build+run:
        providers:libressl? ( dev-libs/libressl:=[>=2.5&<=4.0] )
        providers:openssl?  ( dev-libs/openssl:= )
"

CARGO_SRC_TEST_PARAMS=(
    --bins
    --
    --skip macro_call
)

# Don't let the openssl-sys crate use a vendored openssl
OPENSSL_NO_VENDOR=1

