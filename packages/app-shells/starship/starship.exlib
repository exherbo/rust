# Copyright 2019-2020 Danilo Spinella <danyspin97@protonmail.com>
# Distributed under the terms of the GNU General Public License v2

require github [ user=${PN} ]
require rust cargo [ rust_minimum_version=1.80.0 ]
require bash-completion zsh-completion

SUMMARY="The cross-shell prompt for astronauts"

LICENCES="ISC"
SLOT="0"
MYOPTIONS="
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build+run:
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl:= )
"

src_install() {
    cargo_src_install

    if option bash-completion ; then
        edo target/$(rust_target_arch_name)/release/${PN} completions bash > ${PN}.bash
        dobashcompletion ${PN}.bash
    fi

    if option zsh-completion ; then
        edo target/$(rust_target_arch_name)/release/${PN} completions zsh > _${PN}
        dozshcompletion _${PN}
    fi
}

